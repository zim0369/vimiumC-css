# vimiumC.css

### Custom CSS for vimiumC(not [vimium](https://github.com/zim0369/vimium.css)) vomnibar and vimiumC hint markers.

Copy the vimiumC.css file to the 'custom css' pane in vimiumC's options tab. 

For search engine prefixes in vomnibarC, copy the vimiumC.search file to the 'custom search engines' pane in vimium's options tab.

NOTE: You need to press `gn` to switch between light and dark theme for vomnibar.

![vomnibar on dark background](pictures/vomd.png)
![vomnibar on light background](pictures/voml.png)
![hint markers on dark background](pictures/markd.png)
![hint markers on light background](pictures/markl.png)
